---
permalink: /media-object
layout: left_sidebar
sidebar: components
title: Media Object
---

## Notes on Implementation

We implement Bootstrap's Media Object component, so [Bootstrap media object documentation](https://getbootstrap.com/docs/4.3/components/media-object/) is applicable.

## Example

{% capture example %}

<div class="media">
  <img src="https://via.placeholder.com/64" class="mr-3" alt="Media Image" />
  <div class="media-body">
    <h5 class="mt-0">Media heading</h5>
    Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
  </div>
</div>
{% endcapture %}

{% include example.html content=example myVal="using media" myBtn="first" hiddenSection="hiddenFirst" %}
