---
permalink: /forms
layout: left_sidebar
sidebar: components
title: Forms
---

## Notes on Implementation

We implement Bootstrap's Forms, with some tweaks to the visual appearance to add increased focus styling and a red asterisk for required fields. Also, [ally.js](https://allyjs.io/) is used to add an underline to labels when the corresponding input has focus. The documentation for [Bootstrap Forms](https://getbootstrap.com/docs/4.3/components/forms/) is applicable, along with the Accessibility Notes below.

## Accessibility Notes

Instructions and other helpful information are necessary to avoid errors and help
users successfully complete an online form.

### Custom Form Elements

We do not recommend using the custom Bootstrap form elements (or any custom form element) unless you are very
familiar with the ARIA authoring practices.

### Form Control Attributes and Validation

Form control attributes, such as an input element's `type` attribute, can be used to restrict the input value to help avoid errors. HTML5 input types can also help by giving hints to the browser about what type of keyboard to display on-screen. However, because there are so many usability and accessibility problems when using `<input type="number">`, we recommend using `<input type="text" inputmode="numeric" pattern="[0-9]*">` instead.

Gov.uk has a good article on this if you'd like to read more: [Why the GOV.UK Design System team changed the input type for numbers](https://technology.blog.gov.uk/2020/02/24/why-the-gov-uk-design-system-team-changed-the-input-type-for-numbers/).

Using the correct input type along with other input attributes (pattern, size, maxlength, required, etc.) utilizes HTML's built-in validation features to help avoid input error: [Read the MDN Web Docs on Form Data Validation](https://developer.mozilla.org/en-US/docs/Learn/HTML/Forms/Form_validation)

### HTML Autocomplete Attributes

The autocomplete attributes are only for input fields collecting information
about the user. By using this autocomplete attribute, inputs can be auto-filled by the browser which makes it easier for everyone to fill out a form.
Add the appropriate `autocomplete` attribute (and its corresponding `type` attribute) to each form input field that requires user
information.

[View the HTML specs on the autocomplete attribute](https://html.spec.whatwg.org/multipage/form-control-infrastructure.html#attr-fe-autocomplete)

### Required Fields

Clearly identifying required fields helps prevent user errors and it reduces the
chance that users will neglect to fill out all necessary fields in a form. The general requirements for
identifying required fields are:

- The aria-required="true" indicator or the HTML5 required attribute should be set on the input element.
- A supplemental visible indicator should be available to sighted users (we use a red asterisk).

The example below demonstrates the use of the autocomplete and required attributes.
It also shows the style you can add for a required field, using class="label-required" on the corresponding label
element.
{% capture example %}

<form>
    <div class="form-group">
        <label for="exampleInputEmail1" class="label-required">Email address</label>
        <input type="email" required autocomplete="email" class="form-control" id="exampleInputEmail1"
            aria-describedby="emailHelp" placeholder="Enter email">
        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1" class="label-required">Password</label>
        <input type="password" required autocomplete="current-password" class="form-control" id="exampleInputPassword1"
            placeholder="Password">
    </div>
</form>
{% endcapture %}
{% include example.html content=example myVal="using autocomplete and required attributes" myBtn="first" hiddenSection="hiddenFirst" %}

### Focus Order

Focus order should be logical. Screen reader and keyboard users use the tab key to
navigate the focusable/interactive elements on a page. The sequential order of those elements can help a
user understand the page.

### Labels, Instructions, and Fieldsets

#### HTML Label Element

Using the HTML label element
explicitly is the preferred method when labeling form input fields. Example of using the HTML label
element explicitly:

{% capture example %}

<div class="form-group">
<label for="fname">First Name:</label>
<input type="text" name="fname" id="fname" class="form-control">
</div>
{% endcapture %}
{% include example.html content=example myVal="using the HTML label element explicitly" myBtn="second" hiddenSection="hiddenSecond" %}

Using the HTML label element makes getting into the associated input field easier for
users, as clicking on the label sends focus into that input field.

#### Input Field Instructions

A screen reader user may not hear non-focusable text inside a form if it is not
programmatically associated with an input. Using aria-describedby allows for the programmatic association of
the instructions to the input.

#### Fieldsets

Groups of related form elements should be grouped together in fieldsets with legends.
Example of using a fieldset and legend:

{% capture example %}

<fieldset class="form-group">
    <legend>Radio buttons in a fieldset with a legend</legend>
    <div class="form-check">
        <label class="form-check-label">
            <input type="radio" class="form-check-input" name="optionsRadios" id="optionsRadios1" value="option1"
                checked="">
            Option one is this and that—be sure to include why it's great
        </label>
    </div>
    <div class="form-check">
        <label class="form-check-label">
            <input type="radio" class="form-check-input" name="optionsRadios" id="optionsRadios2" value="option2">
            Option two can be something else and selecting it will deselect option one
        </label>
    </div>
    <div class="form-check disabled">
        <label class="form-check-label">
            <input type="radio" class="form-check-input" name="optionsRadios" id="optionsRadios3" value="option3"
                disabled="">
            Option three is disabled
        </label>
    </div>
</fieldset>

{% endcapture %}
{% include example.html content=example myVal="using a fieldset and legend to group related input fields" myBtn="third" hiddenSection="hiddenThird" %}

Although it is possible to create a group label with `<fieldset>` and
`<legend>`, there is no easy way to create instructions that are associated with a group (adding
aria-describedby to either the `<fieldset>` or `<legend>` element doesn’t work). There are some ways
to do this:

1. Add the instructions to the legend if they are short. Some screen readers
   will read the legend every time the user goes to a new form field in that group. If the legend is
   very long, this can be annoying.
2. Associate the instructions with one of the fields (usually the first field is
   best) within the group, using aria-describedby. This way, the group instructions are only read
   once.
3. Put the instructions before the start of the whole form. This is not the
   most ideal since the user may forget the instructions by the time they get to the group of fields they
   need instructions for.

### Validation Messages

Users need to know of any input errors or if their form submission was successful. We recommend using the [constraint validation API](https://developer.mozilla.org/en-US/docs/Learn/HTML/Forms/Form_validation) to check the validity of the input values and creating custom error messages (do not use browser default error messages).

#### Options for implementing validation messages

1. Upon form submission, send user focus to a validation message as a summary.
   The summary should include a count of errors so that users get a sense of the scope of the
   problem. The summary should also include a link to the first field that has an error. Ensure the summary
   container has a tabindex="-1" and has an ARIA role=”alert”.
2. Upon form submission, send user focus to the first input field with an
   error.
3. Inline (live) validation of error messages. We discourage using this
   method since it is tricky to implement with timing the aria-live announcements of error messages.

#### Must haves for all error messages

- Set aria-invalid="true” on input fields with errors.
- Associate error messages with form fields using aria-describedby so that screen
  reader users know how to fix the problem
- Ensure error messages are visible and adjacent to the inputs so that screen
  magnification users can easily see which messages belong to which fields.

If the form submission causes a new page to load (or the same page to reload):

- Update the page `<title>` to reflect the error or success confirmation
  status. The title could say something like, "There were 2 errors in the form" or "Success! Your
  application has been submitted." The page `<title>` is the first thing screen reader users hear when
  the page loads, so it is the fastest way to give them feedback on the form submission status.
- Provide a quick way to reach the error or success message. For example, provide
  a "skip to main content" link that takes the users to the message.

Below is an example of validating user input and setting the focus to the first field with an error. For more examples of how to validate user input, check out:

[Web Accessibility Tutorials on Validating Input](https://www.w3.org/WAI/tutorials/forms/validation/)

{% capture example %}

<form name="validationForm">
  <div role="alert" id="successMessage" class="alert alert-dismissible alert-success" style="display:none;">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
    Thank you for your submission!</div>
  <div class="form-group">
    <label class="form-control-label label-required" for="email">Email Address</label>
    <input type="email" required class="form-control" name="email" aria-invalid="false" id="email"
      aria-describedby="emailFeedback" autocomplete="email">
    <div id="emailFeedback" class="invalid-feedback"></div>
  </div>
  <div class="form-group">
    <label class="form-control-label label-required" for="username">Username</label>
    <input type="text" required class="form-control" name="username" aria-invalid="false" id="username"
      aria-describedby="usernameFeedback" autocomplete="username">
    <div id="usernameFeedback" class="invalid-feedback"></div>
  </div>
  <button type="button" class="btn btn-secondary" id="submitButton">Submit</button>
</form>

<script>
  document.getElementById("submitButton").addEventListener("click", function () {
    let errors = false
    let allErrors = []
    let messageDiv = document.getElementById('successMessage')
    let emailInputField = document.getElementById("email")
    let emailValid = emailInputField.checkValidity()
    let usernameInputField = document.getElementById("username")
    let usernameValid = usernameInputField.checkValidity()
    if (!emailValid) {
      errors = true
      emailInputField.setAttribute("class", "form-control is-invalid")
      emailInputField.setAttribute('aria-invalid', true)
      let emailError = document.getElementById("emailFeedback")
      emailError.innerHTML = 'Error: Email must be filled out'
      allErrors.push(emailInputField)
    } else {
      emailInputField.setAttribute("class", "form-control is-valid")
      emailInputField.setAttribute('aria-invalid', false)
    }
    if (!usernameValid) {
      errors = true
      usernameInputField.setAttribute('class', "form-control is-invalid")
      usernameInputField.setAttribute('aria-invalid', true)
      let usernameError = document.getElementById("usernameFeedback")
      usernameError.innerHTML = 'Error: Username must be filled out'
      allErrors.push(usernameInputField)
    } else {
      usernameInputField.setAttribute('class', "form-control is-valid")
      usernameInputField.setAttribute('aria-invalid', false)
    }
    if (errors) {
      allErrors[0].focus()
      messageDiv.style.display = 'none'
    } else {
      messageDiv.style.display = 'block'
      emailInputField.setAttribute('class', "form-control is-valid")
      emailInputField.setAttribute('aria-invalid', false)
      usernameInputField.setAttribute('class', "form-control is-valid")
      usernameInputField.setAttribute('aria-invalid', false)
    }
  });
</script>

{% endcapture %}
{% include example.html content=example myVal="using validation messages upon form submittal" myBtn="fourth" hiddenSection="hiddenFourth" %}

### Examples of Form Controls

{% capture example %}

<form>
  <div class="form-group">
    <label for="exampleSelect1">Example select</label>
      <select class="form-control" id="exampleSelect1">
        <option>1</option>
        <option>2</option>
        <option>3</option>
        <option>4</option>
      </select>
  </div>

  <div class="form-group">
    <label for="exampleSelect2">Example multiple select</label>
      <select multiple="" class="form-control" id="exampleSelect2">
        <option>1</option>
        <option>2</option>
        <option>3</option>
        <option>4</option>
        <option>5</option>
      </select>
  </div>
                
  <div class="form-group">
    <label for="exampleTextarea">Example textarea</label>
    <textarea class="form-control" id="exampleTextarea" rows="3"></textarea>
  </div>
                
  <div class="form-group">
    <label for="exampleInputFile">File input</label>
    <input type="file" class="form-control-file" id="exampleInputFile" aria-describedby="fileHelp">
    <small id="fileHelp" class="form-text text-muted">This is some placeholder block-level help text for the above input. It's a bit lighter and easily wraps to a new line. </small>
  </div>
                
  <div class="form-group">
    <fieldset>
     <label class="control-label" for="readOnlyInput">Readonly input</label>
     <input class="form-control" id="readOnlyInput" type="text" placeholder="Readonly input here…" readonly="">
    </fieldset>
  </div>
                
  <div class="form-group has-success">
    <label class="form-control-label" for="inputValid">Valid input</label>
    <input type="text" value="correct value" class="form-control is-valid" id="inputValid">
    <div class="valid-feedback">Success! You've done it.</div>
  </div>

  <div class="form-group has-danger">
    <label class="form-control-label" for="inputInvalid">Invalid input</label>
    <input type="text" value="wrong value" class="form-control is-invalid" id="inputInvalid">
    <div class="invalid-feedback">Sorry, that username's taken. Try another?</div>
  </div>

  <div class="form-group">
    <label class="col-form-label col-form-label-lg" for="inputLarge">Large input</label>
    <input class="form-control form-control-lg" type="text" placeholder=".form-control-lg" id="inputLarge">
  </div>

  <div class="form-group">
    <label class="col-form-label" for="inputDefault">Default input</label>
    <input type="text" class="form-control" placeholder="Default input" id="inputDefault">
  </div>

  <div class="form-group">
    <label class="col-form-label col-form-label-sm" for="inputSmall">Small input</label>
    <input class="form-control form-control-sm" type="text" placeholder=".form-control-sm" id="inputSmall">
  </div>
</form>
{% endcapture %}
{% include example.html content=example myVal="Form Controls" myBtn="controls" hiddenSection="hiddenControls" %}

### Examples of Custom Forms

{% capture example %}

<form>
  <div class="custom-control custom-checkbox">
      <input type="checkbox" class="custom-control-input" id="customCheck1">
      <label class="custom-control-label" for="customCheck1">Check this custom checkbox</label>
  </div>
  <div class="custom-control custom-checkbox">
      <input type="checkbox" class="custom-control-input" id="customCheckDisabled" disabled>
      <label class="custom-control-label" for="customCheckDisabled">Check this disabled checkbox</label>
  </div>
  <fieldset>
      <legend>Custom Radio Buttons</legend>
      <div class="custom-control custom-radio">
          <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
          <label class="custom-control-label" for="customRadio1">Toggle this custom radio</label>
      </div>
      <div class="custom-control custom-radio">
          <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
          <label class="custom-control-label" for="customRadio2">Or toggle this other custom radio</label>
      </div>
      <div class="custom-control custom-radio">
          <input type="radio" id="radio3" name="customRadio" id="customRadioDisabled"
              class="custom-control-input" disabled>
          <label class="custom-control-label" for="customRadioDisabled">Disabled Radio Button</label>
      </div>
  </fieldset>
  <fieldset>
      <legend>Inline Custom Radio Buttons</legend>
      <div class="custom-control custom-radio custom-control-inline">
          <input type="radio" id="customRadioInline1" name="customRadioInline1"
              class="custom-control-input">
          <label class="custom-control-label" for="customRadioInline1">Toggle this custom radio</label>
      </div>
      <div class="custom-control custom-radio custom-control-inline">
          <input type="radio" id="customRadioInline2" name="customRadioInline1"
              class="custom-control-input">
          <label class="custom-control-label" for="customRadioInline2">Or toggle this other custom
              radio</label>
      </div>
  </fieldset>
  <div class="form-group">
      <label for="customSelect">Custom Select</label>
      <select id="customSelect" class="custom-select">
          <option selected>Open this select menu</option>
          <option value="1">One</option>
          <option value="2">Two</option>
          <option value="3">Three</option>
      </select>
  </div>
  <div class="custom-file">
      <input type="file" class="custom-file-input" id="customFile">
      <label class="custom-file-label" for="customFile">Choose file</label>
  </div>
  <div class="custom-control custom-switch">
      <input type="checkbox" class="custom-control-input" id="customSwitch1">
      <label class="custom-control-label" for="customSwitch1">Toggle this switch element</label>
  </div>
  <div class="custom-control custom-switch">
      <input type="checkbox" class="custom-control-input" disabled id="customSwitch2">
      <label class="custom-control-label" for="customSwitch2">Disabled switch element</label>
  </div>
  <div class="form-group">
      <label for="customRange1">Example range</label>
      <input type="range" class="custom-range" id="customRange1">
  </div>
</form>
{% endcapture %}
{% include example.html content=example myVal="Custom Forms" myBtn="custom" hiddenSection="hiddenCustom" %}
