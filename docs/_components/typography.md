---
permalink: /typography
layout: left_sidebar
sidebar: components
title: Typography
---

## Notes on Implementation

We use Bootstrap’s Typography settings. The documentation for [Bootstrap Typography](https://getbootstrap.com/docs/4.3/content/typography/) is applicable.

## Accessibility Notes

For a more inclusive and accessible type scale, use the browser default root font-size (typically 16px) so visitors can customize their browser defaults as needed.

### Best Practices for Typography

- **Headings must be properly nested.** Headings that are properly nested help screen reader users understand the hierarchy of the information on the page. Each page should start with a `<h1>` that explains the purpose of the page and each subsection should have a heading `<h2> ... <h6>` that uniquely describes the content of each section.
- **Do not use the Bootstrap heading classes without giving proper semantic meaning to element.** Bootstrap offers `.h1` through `.h6` CSS classes for when you would like to match the font styling of a heading but cannot use the associated HTML element.

- **Add semantic meaning to headings.** Use ARIA `role="heading"` and the appropriate `aria-level` attribute to give semantic meaning. For example use,
  `<p class="h4" role="heading" aria-level="4"> H4 with ARIA </p>`.

- **Use `<strong>` and `<em>` elements.** Use the `<strong>` and `<em>` elements to give senatic meaning to bold and italic text.
- **Do not use the `<abbr>` element.** The `<abbr>` element is currently not keyboard accessible. Instead, write out the abbreviation or acronym one time on the page to provide additional context. For example, Accessible Rich Internet Applications (ARIA).

## Examples

### Headings

{% capture example %}

<h1>Heading 1</h1>
<h2>Heading 2</h2>
<h3>Heading 3</h3>
<h4>Heading 4</h4>
<h5>Heading 5</h5>
<h6>Heading 6</h6>
{% endcapture %}

{% include example.html content=example myVal="headings" myBtn="Headings" hiddenSection="hiddenHeadings" %}

### Paragraph

{% capture example %}

<p>Yale’s reach is both local and international. It partners with its hometown of New Haven, Connecticut to strengthen the city’s community and economy. And it engages with people and institutions across the globe in the quest to promote cultural understanding, improve the human condition, delve more deeply into the secrets of the universe, and train the next generation of world leaders.</p>
{% endcapture %}

{% include example.html content=example myVal="paragraph" myBtn="Paragraph" hiddenSection="hiddenParagraph" %}

### Blockquote

{% capture example %}

<blockquote class="blockquote">
<p> Yale’s reach is both local and international. It partners with its hometown of New Haven, Connecticut to strengthen the city’s community and economy. And it engages with people and institutions across the globe in the quest to promote cultural understanding, improve the human condition, delve more deeply into the secrets of the universe, and train the next generation of world leaders.</p>
<p class="blockquote-footer">Peter Salovey, Yale's 23rd president.</p>
</blockquote>
{% endcapture %}

{% include example.html content=example myVal="blockquote" myBtn="Blockquote" hiddenSection="hiddenBlockquote" %}

### Lead

{% capture example %}

<p class="lead">Yale’s reach is both local and international. It partners with its hometown of New Haven, Connecticut to strengthen the city’s community and economy. And it engages with people and institutions across the globe in the quest to promote cultural understanding, improve the human condition, delve more deeply into the secrets of the universe, and train the next generation of world leaders.</p>
{% endcapture %}

{% include example.html content=example myVal="Lead" myBtn="Lead" hiddenSection="hiddenLead" %}

### Formatting Elements

{% capture example %}

<p>This is how we <a href="#">style links by default in body text</a></p>
<p>
<small>This line of text is meant to be treated as fine print.</small>
</p>
<p><strong>This line is rendered as bold text</strong>.</p>
<p><em>This line is rendered as italicized text</em>.</p>
<p>An abbreviation of the word attribute is <abbr title="attribute">attr</abbr>.</p>
{% endcapture %}

{% include example.html content=example myVal="Formatting" myBtn="Formatting" hiddenSection="hiddenFormatting" %}
