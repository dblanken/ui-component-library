---
permalink: /input-group
layout: left_sidebar
sidebar: components
title: Input Group
---

## Notes on Implementation

We implement Bootstrap's Input Group component, so [Bootstrap Input Group documentation](https://getbootstrap.com/docs/4.3/components/input-group/) is applicable.

## Accessibility Notes

Many of the input group examples in Bootstrap use placeholder values. The placeholder attribute is highly discouraged, as it disappears upon user input and is usually of insufficient color contrast. It is best practice to always use a `<label>` element for form controls. Any additional information (like that is inline with the form control) can be given an `id` value. An `aria-describedby` attribute on the form control can then be assigned to that id value. See the example below of a form control using a label of "Your vanity URL" and an `aria-describedby` on the input field that references the extra information of "https://example.com/users/".

## Examples

{% capture example %}
<label for="basic-url">Your vanity URL</label>

<div class="input-group mb-3">
  <div class="input-group-prepend">
    <span class="input-group-text" id="basic-addon1">https://example.com/users/</span>
  </div>
  <input type="text" class="form-control" id="basic-url" aria-describedby="basic-addon1">
</div>

<label for="amount">Amount (to the nearest dollar)</label>

<div class="input-group mb-3">
  <div class="input-group-prepend">
    <span class="input-group-text" id="basic-addon2">$</span>
  </div>
  <input type="text" class="form-control" id="amount" aria-describedby="basic-addon2">
   <div class="input-group-append">
    <span class="input-group-text">.00</span>
  </div>
</div>

{% endcapture %}

{% include example.html content=example myVal="using input groups" myBtn="first" hiddenSection="hiddenFirst" %}

{% capture example %}
<label for="small-example">Small sized input group</label>

<div class="input-group input-group-sm mb-3">
  <div class="input-group-prepend">
    <span class="input-group-text" id="inputGroup-sizing-sm">Small</span>
  </div>
  <input type="text" class="form-control" id="small-example" aria-describedby="inputGroup-sizing-sm">
</div>

<label for="default-example">Default sized input group</label>

<div class="input-group mb-3">
  <div class="input-group-prepend">
    <span class="input-group-text" id="inputGroup-sizing-default">Default</span>
  </div>
  <input type="text" class="form-control" id="default-example" aria-describedby="inputGroup-sizing-default">
</div>

<label for="large-example">Large sized input group</label>

<div class="input-group input-group-lg">
  <div class="input-group-prepend">
    <span class="input-group-text" id="inputGroup-sizing-lg">Large</span>
  </div>
  <input type="text" class="form-control" id="large-example" aria-describedby="inputGroup-sizing-lg">
</div>
{% endcapture %}

{% include example.html content=example myVal="using input groups of different sizes" myBtn="second" hiddenSection="hiddenSecond" %}
