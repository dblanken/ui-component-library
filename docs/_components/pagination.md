---
permalink: /pagination
layout: left_sidebar
sidebar: components
title: Pagination
---

## Notes on Implementation

We implement Bootstrap's Pagination component, with some tweaks to the visual appearance. The documentation for [Bootstrap Pagination](https://getbootstrap.com/docs/4.3/components/pagination/) is applicable.

## Accessibility Notes

To meet accessibility standards, it is recommended to provide a descriptive `aria-label` for the navigation elements to indicate its purpose. For example, if pagination is used to navigate between lists of search results, the appropriate label could be `aria-label="Search results pages"`. Also, adding `aria-current="page"` to the active/current page is helpful for assistive technology to inform users of the currently selected page.

## Examples

### With Active States

{% capture example %}

<nav aria-label="Page navigation example with active states">
  <ul class="pagination">
    <li class="page-item"><a class="page-link" href="#" tabindex="-1">Previous</a></li>
    <li class="page-item"><a class="page-link" href="#">1</a></li>
    <li class="page-item active"><a class="page-link" href="#" aria-current="page">2</a></li>
    <li class="page-item"><a class="page-link" href="#">3</a></li>
    <li class="page-item"><a class="page-link" href="#">Next</a></li>
  </ul>
</nav>
{% endcapture %}
{% include example.html content=example myVal="using Pagination with Active States" myBtn="first" hiddenSection="hiddenFirst" %}

### Without Active States

{% capture example %}

<nav aria-label="Page navigation example without active states">
<ul class="pagination">
    <li class="page-item"><a class="page-link" href="#" tabindex="-1">Previous</a></li>
    <li class="page-item"><a class="page-link" href="#">1</a></li>
    <li class="page-item"><a class="page-link" href="#">2</a></li>
    <li class="page-item"><a class="page-link" href="#">3</a></li>
    <li class="page-item"><a class="page-link" href="#">Next</a></li>
  </ul>
</nav>
{% endcapture %}
{% include example.html content=example myVal="using Pagination without Active States" myBtn="second" hiddenSection="hiddenSecond" %}
