---
permalink: /modal-dialogs
layout: left_sidebar
sidebar: components
title: Modal Dialogs
---

## Notes on Implementation

We implement Bootstrap's Modal component, with some tweaks to the visual appearance. The documentation for [Bootstrap Modals](https://getbootstrap.com/docs/4.3/components/modal/) is applicable.

## Accessibility Notes

Modal dialogs will have a `role="dialog"` or `role="alertdialog"` on their container div element. The dialog's name must be designated by an aria-labelledby reference to the first heading in the dialog or an aria-label. An aria-describedby attribute can also be used if more information is needed to be conveyed.

- A regular dialog (`role="dialog"`) should be used for form-like content in a dialog.
- An alert dialog (`role="alertdialog"`) should be brief, and all text should be associated with the dialog, using aria-labelledby and/or aria-describedby.

## Example

{% capture example %}
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
Launch demo modal
</button>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title" id="exampleModalLabel">Modal title</h1>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <h2>Modal body heading</h2>
                <p>Modal body text description</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
{% endcapture %}
{%- include example.html content=example myVal="modal dialog" myBtn="modal" hiddenSection="hiddenModal" -%}
