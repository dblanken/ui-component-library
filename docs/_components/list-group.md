---
permalink: /list-group
layout: left_sidebar
sidebar: components
title: List Group
---

## Notes on Implementation

We do not use Bootstrap's List Group component. We implement our list groups with fewer features. In particular, we <strong>do not</strong> implement contextual classes for list groups. We also do not implement Bootstrap's tabs JavaScript. Otherwise, the documentation for [Bootstrap List Groups](https://getbootstrap.com/docs/4.3/components/list-group/) is applicable.

## Accessibility Notes

Creating disabled list group items has accessibility concerns developers should keep in mind.

Only create disabled list group items using the `disabled` attribute, never the `.disabled` class, and only use the `disabled` attribute on buttons and other form inputs.

Semantically in HTML, there is no such thing as a "disabled" link. If you wish to indicate that a particular list group item could be a link, include text to that effect.

## Example

{% capture example %}

<div class="list-group">
    <div class="list-group-item">A normal list group item</div>
    <a href="#" class="list-group-item list-group-item-action">List Group Action Item: Link</a>
    <button class="list-group-item list-group-item-action">List Group Action Item: Button</button>
    <button disabled class="list-group-item list-group-item-action">List Group Action Item: Button, Disabled</button>
    <div class="list-group-item text-muted font-italic">List group item, not a button</div>
</div>
{% endcapture %}
{% include example.html content=example myVal="list groups" myBtn="lg" hiddenSection="hiddenLg" %}
