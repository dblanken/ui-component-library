---
permalink: /jumbotron
layout: left_sidebar
sidebar: components
title: Jumbotron
---

## Notes on Implementation

We implement Bootstrap's Jumbotron component, so [Bootstrap Jumbotron documentation](https://getbootstrap.com/docs/4.3/components/jumbotron/) is applicable.

## Accessibility Notes

When using jumbotron, ensure that the jumbotron's heading level does not break the existing heading hierarchy. If additional styling is necessary, make sure to use the `.display-` classes or any of the other utility classes that are available in Bootstrap.

For jumbotron links, be sure to use `<a>` tags with `.btn` classes, rather than `<button>` elements.

## Example

{% capture example %}

<div class="jumbotron">
  <h2 class="display-4 mt-0">Hello, world!</h2>
  <p>It uses utility classes for typography and spacing to space content out within the larger container.</p>
  <a class="btn btn-primary" href="#">Learn more</a>
</div>
{% endcapture %}

{% include example.html content=example myVal="using jumbotron" myBtn="first" hiddenSection="hiddenFirst" %}
