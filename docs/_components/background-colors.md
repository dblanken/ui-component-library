---
permalink: /background-colors
layout: left_sidebar
sidebar: components
title: Background Colors
---

## Notes on Implementation

To generate background variant styles, we use the mixin, `accessible-bg-variant`, rather than Bootstrap's `bg-variant`. The `accessible-bg-variant` mixin will set the text, heading, and link color to either white or black (depending on the color contrast ratio of the text with the selected background color). A color contrast ratio of at least 4.5:1 is required for normal-sized text to ensure accessibility. A white background can be implemented with the class `bg-white`, which uses the default (blue) colors for headings and links (i.e., the class `bg-white` only changes the background color to white).

{% capture example %}
{% for color in site.data.theme-colors %}

<div class="bg-{{ color.name }} pl-4 p-2">
<h3>Background Color class="bg-{{color.name}}"</h3>
<p>This is a background style based on the <strong>{{color.name}}</strong> theme color and <a href="#">this is a link</a>. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas facilisis sapien lorem, rhoncus ultricies orci iaculis vitae. Phasellus ipsum erat, imperdiet non porttitor vitae, eleifend sed ex.</p>
</div>
{% endfor %}
{% endcapture %}
{% include example.html content=example myVal="background color utilities" myBtn="bg" hiddenSection="hiddenBg" %}
