---
permalink: /badges
layout: left_sidebar
sidebar: components
title: Badges
---

## Notes on Implementation

We do not use Bootstrap's Badges component, but implement our own with increased color contrast. The documentation for [Bootstrap Badges](https://getbootstrap.com/docs/4.3/components/badge/) is applicable, along with the Accessibility Notes below.

## Accessibility Notes

Note that depending on how they are used, badges may be confusing for users of screen readers and similar assistive technologies. While the styling of badges provides a visual cue as to their purpose, these users will simply be presented with the content of the badge. Depending on the specific situation, these badges may seem like random additional words or numbers at the end of a sentence, link, or button.

## Examples

### Default Badge Styles

A basic badge can be created with the `.badge` class. Badges scale to match the size of the immediate parent element by using relative font sizing and em units.

{% capture example %}
{% for color in site.data.theme-colors %}
<span class="badge badge-{{ color.name }}">{{ color.name }}</span>{% endfor %}
{% endcapture %}

{% include example.html content=example myVal="using default badge styles" myBtn="first" hiddenSection="hiddenFirst" %}

### Pill Badges

Use the .badge-pill modifier class to make badges more rounded (with a larger border-radius and additional horizontal padding). Useful if you miss the badges from Bootstrap v3.

{% capture example %}
{% for color in site.data.theme-colors %}
<span class="badge badge-pill badge-{{ color.name }}">{{ color.name }}</span>{% endfor %}
{% endcapture %}

{% include example.html content=example myVal="using pill badge styles" myBtn="second" hiddenSection="hiddenSecond" %}

### Links

Using the contextual .badge-\* classes on an <a> element quickly provide actionable badges with hover and focus states.

{% capture example %}
{% for color in site.data.theme-colors %}
<a href="#" class="badge badge-{{ color.name }}">{{ color.name }}</a>{% endfor %}
{% endcapture %}

{% include example.html content=example myVal="using linked-badge styles" myBtn="third" hiddenSection="hiddenThird" %}
