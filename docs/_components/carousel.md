---
permalink: /carousel
layout: left_sidebar
sidebar: components
title: Carousel
---

## Notes on Implementation

We implement Bootstrap’s Carousel component, with some tweaks to the visual appearance and functionality to make it accessible (following the ARIA authoring practices for a basic carousel). We do not recommend following the Bootstrap documention, but rather following our code below for the type of carousel you would want to implement.

## Accessibility Notes

The default Bootstrap carousel will rotate automatically on page load. Since the default Bootstrap carousel does not have a pause, stop, or hide button, this violates WCAG success criterion 2.2.2: Pause, Stop, Hide. All of the carousels below have a default interval setting of false to prevent automatic cycling of the carousel images/slides.

Copy the code below exactly (for the type of carousel you want) and then replace the following attributes with your own that make sense for each image/slide. Also, add as many or as little images as needed.

The `aria-label` attribute on the `section` element is a way to label your carousel. A screen reader user will hear the label and then the role of "carousel". Without the label, the role is not read.

Each slide container has the attribute `role = "group"` with the property `aria-roledescription` set to "slide".

Each slide needs an accessible name. If a slide has a visible label, its accessible label is provided by the property `aria-labelledby` on the slide container set to the ID of the element containing the visible label.
Otherwise, an accessible label is provided by the property `aria-label` set on the slide container.

Each image/slide will need to be included via the `img`'s `src` attribute. Also, include an `alt` attribute that describes the information you want to relay to the user for each image.

The slide number or position out of the total number of slides is helpful to include for all users. This lets users know how many slides they have to cycle through.

Include more information if needed. Links can be included only on the carousel with captions below the slides.

## Example of Captions Below Carousel

{% capture example %}

<section class="bd-example" aria-roledescription="carousel" aria-label="visit Yale">
  <div id="carouselExampleCaptions" class="carousel slide">
    <div class="carousel-inner" aria-live="polite">
      <div class="carousel-item active" role="group"
           aria-roledescription="slide"
           aria-label="1 of 3">
        <img src="{{ '/assets/img/1.png' | relative_url }}" class="d-block w-100" alt="Yale">
        <div id="firstSlide" class="carousel-caption d-none">
          <em class="text-muted">Slide 1 of 3</em>
          <p>
            <a href="https://www.yale.edu/about-yale/visiting">Visit Yale</a>
          </p>
        </div>
      </div>
      <div class="carousel-item" role="group"
           aria-roledescription="slide"
           aria-label="2 of 3">
        <img src="{{ '/assets/img/2.png' | relative_url }}" class="d-block w-100" alt="Sterling Memorial Library">
        <div class="carousel-caption d-none">
          <em class="text-muted">Slide 2 of 3</em>
          <p>
            <a href="https://web.library.yale.edu/building/sterling-library">Visit Sterling Memorial Library</a>
          </p>
        </div>
      </div>
      <div class="carousel-item" role="group"
           aria-roledescription="slide"
           aria-label="3 of 3">
        <img src="{{ '/assets/img/3.png' | relative_url }}" class="d-block w-100" alt="Woolsey Hall">
        <div class="carousel-caption d-none">
          <em class="text-muted">Slide 3 of 3</em>
          <p>
            <a href="https://music.yale.edu/concerts/venues/woolsey/">Visit Woolsey Hall</a>
          </p>
        </div>
      </div>
    </div>
    <div class="carousel-control-prev">
      <button data-target="#carouselExampleCaptions" data-slide="prev">
        <span class="carousel-control-prev-icon mt-2" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </button>
    </div>
    <div class="carousel-control-next">
      <button data-target="#carouselExampleCaptions" data-slide="next">
        <span class="carousel-control-next-icon mt-2" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </button>
    </div>
  </div>
  <div id="displayText" class="text-center" aria-live="polite"></div>
</section>

{% endcapture %}
{% include example.html content=example myVal="captions below carousel" myBtn="first" hiddenSection="hiddenFirst"
%}

## Example of Captions On Carousel

{% capture example %}

<section class="bd-example" aria-roledescription="carousel" aria-label="Yale">
  <div id="carouselExampleCaptionsOnSlides" class="carousel slide" data-interval="false">
    <div class="carousel-inner" aria-live="polite">
      <div class="carousel-item active" role="group"
           aria-roledescription="slide"
           aria-label="1 of 3">
        <img src="{{ root }}assets/img/1.png" class="d-block w-100" alt="Yale">
        <div class="carousel-caption">
          <p class="sr-only">Slide 1 of 3</p>
          <div>The beautiful Yale campus. </div>
        </div>
      </div>
      <div class="carousel-item" role="group"
           aria-roledescription="slide"
           aria-label="2 of 3">
        <img src="{{ root }}assets/img/2.png" class="d-block w-100" alt="Sterling Memorial Library">
        <div class="carousel-caption">
          <p class="sr-only">Slide 2 of 3</p>
          <div>The nave of Sterling Memorial Library. </div>
        </div>
      </div>
      <div class="carousel-item" role="group"
           aria-roledescription="slide"
           aria-label="3 of 3">
        <img src="{{ root }}assets/img/3.png" class="d-block w-100" alt="Woolsey Hall">
        <div class="carousel-caption">
          <p class="sr-only">Slide 3 of 3</p>
          <div>The ceiling of Woolsey Hall.</div>
        </div>
      </div>
    </div>
    <div class="carousel-control-prev">
      <button data-target="#carouselExampleCaptionsOnSlides" data-slide="prev">
        <span class="carousel-control-prev-icon mt-2" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </button>
    </div>
    <div class="carousel-control-next">
      <button data-target="#carouselExampleCaptionsOnSlides" data-slide="next">
        <span class="carousel-control-next-icon mt-2" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </button>
    </div>
  </div>
</section>

{% endcapture %}
{% include example.html content=example myVal="captions on carousel" myBtn="second" hiddenSection="hiddenSecond"
%}

## Example of No Visible Captions

{% capture example %}

<section class="bd-example" aria-roledescription="carousel" aria-label="images of Yale">
  <div id="carouselExampleNoCaptions" class="carousel slide" data-interval="false">
    <div class="carousel-inner" aria-live="polite">
      <div class="carousel-item active" role="group"
           aria-roledescription="slide"
           aria-label="1 of 3">
        <img src="{{ root }}assets/img/1.png" class="d-block w-100" alt="Yale">
        <div class="carousel-caption sr-only">
          <strong>Slide 1 of 3</strong>
          <p>The beautiful Yale campus. </p>
        </div>
      </div>
      <div class="carousel-item" role="group"
           aria-roledescription="slide"
           aria-label="2 of 3">
        <img src="{{ root }}assets/img/2.png" class="d-block w-100" alt="Sterling Memorial Library">
        <div class="carousel-caption sr-only">
          <strong>Slide 2 of 3</strong>
          <p>The nave of Sterling Memorial Library. </p>
        </div>
      </div>
      <div class="carousel-item" role="group"
           aria-roledescription="slide"
           aria-label="3 of 3">
        <img src="{{ root }}assets/img/3.png" class="d-block w-100" alt="Woolsey Hall">
        <div class="carousel-caption sr-only">
          <strong>Slide 3 of 3</strong>
          <p>The ceiling of Woolsey Hall.</p>
        </div>
      </div>
    </div>
    <div class="carousel-control-prev">
      <button data-target="#carouselExampleNoCaptions" data-slide="prev">
        <span class="carousel-control-prev-icon mt-2" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </button>
    </div>
    <div class="carousel-control-next">
      <button data-target="#carouselExampleNoCaptions" data-slide="next">
        <span class="carousel-control-next-icon mt-2" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </button>
    </div>
  </div>
</section>

{% endcapture %} {% include example.html content=example myVal="no visible captions" myBtn="third" hiddenSection="hiddenThird"
%}
