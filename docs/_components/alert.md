---
permalink: /alerts
layout: left_sidebar
sidebar: components
title: Alerts
---

## Notes on Implementation

We do not use Bootstrap's Alerts component, but implement our own with increased color contrast. The documentation for [Bootstrap Alerts](https://getbootstrap.com/docs/4.3/components/alerts/) is applicable.

## Accessibility Notes

If the purpose of the alert is to provide a status message, such as a form error or success message, be sure to include `role="alert"` on the div. Assistive technology such as screen readers will handle status messages with `role="alert"` differently depending on how the attribute is applied (i.e. on page load or dynamically). Read Mozilla's page on [Using the Alert Role](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/ARIA_Techniques/Using_the_alert_role) for more detailed guidance on using alerts.

## Adding the Icons

The Alert component uses Font Awesome v4.7 Icons. [Add the Font Awesome CDN to your project](https://fontawesome.com/v4.7.0/).

## Dismissible Alerts

The Alert component can be turned into a dismissible alert. Follow the [Bootstrap Dismissing Alerts documentation](https://getbootstrap.com/docs/4.3/components/alerts/#dismissing) to add the behavior.

**Note: Use dismissible alerts with care!** Users will not be able to view or hear the message after dismissing (without refreshing the page). Keep important messages on the page at all times.
{% capture example %}

<div class="alert alert-success alert-dismissible fade show" role="alert">
  <h3 class="alert-heading">Dismissible Alert in <strong>success</strong> Theme Color</h3>
    <p>This is an alert style based on the <strong>success</strong> theme color and <a href="#">this is a link</a>. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>

<h2 id="examples"> Examples</h2>
{% for color in site.data.theme-colors %}
<div class="alert alert-{{ color.name }}">
    <h3 class="alert-heading">Sample Heading in <strong>{{color.name}}</strong> Theme Color</h3>
    <p>This is an alert style based on the <strong>{{color.name}}</strong> theme color and <a href="#">this is a
            link</a>. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas facilisis sapien lorem, rhoncus
        ultricies orci iaculis vitae. Phasellus ipsum erat, imperdiet non porttitor vitae, eleifend sed ex.</p>
</div>

{% endfor %}
{% endcapture %}
{% include example.html content=example myVal="alerts" myBtn="alerts" hiddenSection="hiddenAlerts" %}
