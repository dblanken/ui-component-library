---
permalink: /navs
layout: left_sidebar
sidebar: components
title: Navs and Navbar
---

## Notes on Implementation

We implement Bootstrap's Navs and Navbar components, with some tweaks to the visual appearance of the text to increase color contrast and include an underline. The documentation for [Bootstrap Navs](https://getbootstrap.com/docs/4.3/components/navs/) and [Bootstrap Navbar](https://getbootstrap.com/docs/4.3/components/navbar/) is applicable.

## Accessibility Notes

Add a role="navigation" to the parent container of the <ul>, or wrap a <nav> element around the whole navigation.

If more than one navigation menu is on a page, a label is necessary to distinguish between them. Use an aria-label or aria-labelledby attribute to provide the label on the <nav> element.

Use the aria-current="page" attribute on the current/active <a> element to indicate the current page in the menu.

Links must look like links. Color alone must not be used for distinguishing links (this is why an underline is often recommended). Navs and navbars should be styled consistently in terms of their behavior, appearance, and location on pages across a site. If this is done well, the underline on link elements is not necessary and can be removed. A navbar example without the link underlines is given below.

## Nav Examples

### Left-Aligned Nav

{% capture example %}

<nav aria-label="Left-Aligned">
<ul class="nav">
  <li class="nav-item">
    <a class="nav-link active" href="#" aria-current="page">Active</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#">Link</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#">Link</a>
  </li>
  <li class="nav-item">
    <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
  </li>
</ul>
</nav>
{% endcapture %}
{% include example.html content=example myVal="a left-aligned nav" myBtn="leftAlignedNav" hiddenSection="hiddenLeftAlignedNav" %}
<hr />

### Centered Nav

{% capture example %}

<nav aria-label="Centered">
<ul class="nav justify-content-center">
  <li class="nav-item">
    <a class="nav-link active" href="#" aria-current="page">Active</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#">Link</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#">Link</a>
  </li>
  <li class="nav-item">
    <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
  </li>
</ul>
</nav>
{% endcapture %}
{% include example.html content=example myVal="a centered nav" myBtn="centeredNav" hiddenSection="hiddenCenteredNav" %}
<hr />

### Right-Aligned Nav

{% capture example %}

<nav aria-label="Right-Aligned">
<ul class="nav justify-content-end">
  <li class="nav-item">
    <a class="nav-link active" href="#" aria-current="page">Active</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#">Link</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#">Link</a>
  </li>
  <li class="nav-item">
    <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
  </li>
</ul>
</nav>
{% endcapture %}
{% include example.html content=example myVal="a right-aligned nav" myBtn="rightAlignedNav" hiddenSection="hiddenRightAlignedNav" %}
<hr />

### Vertical Nav

{% capture example %}

<nav aria-label="Vertical">
<ul class="nav flex-column">
  <li class="nav-item">
    <a class="nav-link active" href="#" aria-current="page">Active</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#">Link</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#">Link</a>
  </li>
  <li class="nav-item">
    <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
  </li>
</ul>
</nav>
{% endcapture %}
{% include example.html content=example myVal="a vertical nav" myBtn="verticalNav" hiddenSection="hiddenVerticalNav" %}

<hr />

### Tabs

{% capture example %}

<ul class="nav nav-tabs">
  <li class="nav-item">
    <a class="nav-link active" href="#">Active</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#">Link</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#">Link</a>
  </li>
  <li class="nav-item">
    <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
  </li>
</ul>
{% endcapture %}
{% include example.html content=example myVal="the nav-tabs class" myBtn="tabs" hiddenSection="hiddenTabs" %}
<hr />

### Pills

{% capture example %}

<ul class="nav nav-pills">
  <li class="nav-item">
    <a class="nav-link active" href="#">Active</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#">Link</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#">Link</a>
  </li>
  <li class="nav-item">
    <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
  </li>
</ul>
{% endcapture %}
{% include example.html content=example myVal="the nav-pills class" myBtn="pills" hiddenSection="hiddenPills" %}
<hr />

### Fill

(Elements are not equal-width and nav fills entire width.)
{% capture example %}

<ul class="nav nav-pills nav-fill">
  <li class="nav-item">
    <a class="nav-link active" href="#">Active</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#">Much longer nav link</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#">Link</a>
  </li>
  <li class="nav-item">
    <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
  </li>
</ul>
{% endcapture %}
{% include example.html content=example myVal="the nav-fill class" myBtn="fills" hiddenSection="hiddenFills" %}

<hr />

### Justify

(Elements are equal width and nav fills entire width.)
{% capture example %}

<ul class="nav nav-pills nav-justified">
  <li class="nav-item">
    <a class="nav-link active" href="#">Active</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#">Much longer nav link</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#">Link</a>
  </li>
  <li class="nav-item">
    <a class="nav-link disabled" href="#">Disabled</a>
  </li>
</ul>
{% endcapture %}
{% include example.html content=example myVal="the nav-justified class" myBtn="justified" hiddenSection="hiddenJustified" %}
<hr />

## Navbar Example

{% capture example %}

<nav class="navbar navbar-expand-lg navbar-light bg-light" aria-label="Main">
  <a class="navbar-brand" href="#">Navbar</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Link</a>
      </li>
      <li class="nav-item">
        <a class="nav-link disabled" href="#">Disabled</a>
      </li>
    </ul>
  </div>
</nav>
{% endcapture %}
{% include example.html content=example myVal="a navbar" myBtn="navbar" hiddenSection="hiddenNavbar" %}

## Navbar Example without underlines

{% capture example %}

<nav class="navbar navbar-expand-lg navbar-light bg-light" aria-label="Main">
  <a class="navbar-brand text-decoration-none" href="#">Navbar</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link text-decoration-none" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link text-decoration-none" href="#">Link</a>
      </li>
      <li class="nav-item">
        <a class="nav-link disabled text-decoration-none" href="#">Disabled</a>
      </li>
    </ul>
  </div>
</nav>
{% endcapture %}
{% include example.html content=example myVal="a navbar without underlines" myBtn="navbarNoUnderline" hiddenSection="hiddenNavbarNoUnderline" %}
