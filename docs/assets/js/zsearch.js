---
---
// adapted from https://avada.io/shopify/devdocs/insert-the-search-box-on-your-jekyll-site-using-lunr-js.html
function displaySearchResults(results, searchIndex, searchTerms) {
    var searchResults = document.getElementById('search-results');
    var searchStatus = document.getElementById('results');
    document.title = `Search for ${searchTerms} | {{site.title}}`;

    if (results.length) {
        var appendString = '<ul>';

        if (results.length==1) {
            searchStatus.innerHTML = `<h1>Search for "${searchTerms}"</h1><h2>1 Result</h2>`
        } else {
            searchStatus.innerHTML = `<h1>Search for "${searchTerms}"</h1><h2>${results.length} Results</h2>`
        }
        
        for (var i = 0; i < results.length; i++) {  // Iterate over the results
            var item = searchIndex[results[i].ref];
            appendString += '<li><h3><a href="' + item.url + '">' + item.title + '</a></h3>';
            appendString += '<p>' + item.content.substring(0, 150) + '...</p></li>';
        }

        searchResults.innerHTML = appendString + '</ul>';
    } else {
        searchResults.innerHTML = `<h1>Search for "${searchTerms}"</h1><h2>No results found</h2>`;
    }
} 

function getQueryVariable(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split('&');

    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');

        if (pair[0] === variable) {
            return decodeURIComponent(pair[1].replace(/\+/g, '%20'));
        }
    }
}

var searchTerms = getQueryVariable('query');

var searchIndex = [{% for page in site.pages %}{% unless page.url contains ".json" or page.url contains ".xml" or page.url contains "search" %}
    {
      "title"   : "{{ page.title | escape }}",
      "content" : "{{ page.content | strip_html | normalize_whitespace | escape }}",
      "url"     : "{{ page.url | relative_url }}"
    },{% endunless %}{% endfor %}{% for component in site.components %}{% if component.title %}
    {
      "title"   : "{{ component.title | escape }}",
      "content" : "{{ component.content | strip_html | normalize_whitespace | escape }}",
      "url"     : "{{ component.url | relative_url }}"
    }{% unless forloop.last %},{% endunless %}{% endif %}{% endfor %}]

if (searchTerms) {
    document.getElementById('search-input').setAttribute("value", searchTerms);

    // Initalize lunr with the fields it will be searching on.
    // A boost of 10 on title is used to indicate matches on this field are more important.
    var idx = lunr(function () {
        this.field('title', { boost: 10 });
        this.field('content');
        this.field('url');
 
        searchIndex.forEach(function (doc, index) {
            doc.id = index;
            this.add(doc)
        }, this)
    })

    var results = idx.search(searchTerms); // Get lunr to perform a search
    displaySearchResults(results, searchIndex, searchTerms); // We'll write this in the next section
} else {
    var searchStatus = document.getElementById('results');
    searchStatus.innerHTML = `<div class="bg-white" style="min-height: 400px;"><h1>Search for ""</h1><h2>No results</h2></div>`;
  
}
    
 
