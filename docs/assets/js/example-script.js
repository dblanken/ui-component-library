(function() {
  'use strict'
  
  document.myButton = (event) => {
    let count = event.target.name
    count++
    const btn = document.getElementById(event.target.id)
    const sectionToOpen = document.getElementById(event.target.value)
    if (count % 2 === 0) {
      sectionToOpen.style.display = 'none'
      btn.setAttribute('aria-expanded', false)
    } else {
      sectionToOpen.style.display = ''
      btn.setAttribute('aria-expanded', true)
    }
    event.target.name = count
  }

  const preElements = document.querySelectorAll('pre');
  [...preElements].forEach( el => {
    el.setAttribute("tabindex", "0")
  });
  
  })()