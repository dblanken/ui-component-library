const gulp = require('gulp');
const csso = require('gulp-csso');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const rename = require('gulp-rename');
const concat = require('gulp-concat');
const terser = require('gulp-terser');
const zip = require('gulp-zip');

const jsFiles = [
  'node_modules/focus-visible/dist/focus-visible.js',
  'src/js/vendor/accessible-flyout-menu.js',
  'src/js/scripts.js',
];

function distStyles() {
  return gulp
    .src('src/scss/main.scss')

    .pipe(
      sass({
        outputStyle: 'nested',
        precision: 10,
        includePaths: ['.', './node_modules/'],
        onError: console.error.bind(console, 'Sass error:'),
      })
    )
    .pipe(
      autoprefixer({
        cascade: false,
      })
    )
    .pipe(rename('yale-ui.css'))
    .pipe(gulp.dest('./dist/css'))

    .pipe(csso())
    .pipe(rename('yale-ui.min.css'))
    .pipe(gulp.dest('./dist/css'));
}

function docsStyles() {
  return gulp
    .src('./docs/assets/scss/yale-ui-docs.scss')

    .pipe(
      sass({
        outputStyle: 'nested',
        precision: 10,
        includePaths: ['.', './node_modules/'],
        onError: console.error.bind(console, 'Sass error:'),
      })
    )
    .pipe(
      autoprefixer({
        cascade: false,
      })
    )
    .pipe(gulp.dest('./docs/assets/css'))
    .pipe(csso())
    .pipe(rename('yale-ui-docs.min.css'))
    .pipe(gulp.dest('./docs/assets/css'));
}

function scripts() {
  return gulp
    .src(jsFiles)
    .pipe(concat('yale-ui-scripts.js'))
    .pipe(gulp.dest('./dist/js'))
    .pipe(gulp.dest('./docs/assets/js'))

    .pipe(terser())
    .pipe(rename('yale-ui-scripts.min.js'))
    .pipe(gulp.dest('./dist/js'))
    .pipe(gulp.dest('./docs/assets/js'));
}

function moveScss() {
  return gulp.src('./src/scss/**').pipe(gulp.dest('./dist/scss'));
}

function zippit() {
  return gulp
    .src('./dist/**')
    .pipe(zip('ui-component-library.zip'))
    .pipe(gulp.dest('./docs/assets/distribution'));
}

async function compile() {
  await gulp.series([distStyles, docsStyles, scripts, moveScss, zippit])();
}

exports.default = compile;
