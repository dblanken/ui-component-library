/**
 * ------------------------------------------------------------------------
 * Additional code to make Bootstrap carousels accessible!
 * ------------------------------------------------------------------------
 */
$(document).ready(function () {
  //set the interval to false so the carousels do not rotate automatically:
  $('.carousel').data('interval', false);

  //First Carousel with Captions Below, set first slide's caption on page load:
  let displayText = document.getElementById('displayText');
  let firstSlide = document.getElementById('firstSlide');
  displayText.innerHTML = firstSlide.innerHTML;
  //First Carousel with Captions Below, set the current slide's caption to the div below the images/slides:
  $('#carouselExampleCaptions').on(
    'slid.bs.carousel',
    function (relatedTarget) {
      displayText.innerHTML = relatedTarget.relatedTarget.children[1].innerHTML;
    }
  );
});
/** ---------------------------------------------------------- */

/**
 * ------------------------------------------------------------------------
 * Additional code to initialize tooltips and popovers
 * ------------------------------------------------------------------------
 */

$(document).ready(function () {
  $('[data-toggle="popover"]').popover();
  $('[data-toggle="tooltip"]').tooltip();
});
